import { Injectable } from '@angular/core';
import { SafeHttpProvider } from '../safehttp/safehttp';
import { config } from '../../config/config';
import { RequestOptions, Headers } from '@angular/http';


@Injectable()
export class AuthenticationProvider {

  constructor(public http: SafeHttpProvider) {}
  getHeaders() {
    let headers = new Headers();
  
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
    return new RequestOptions({ headers:headers});
  
  }

  register(details){
    return this.http.post(config.base_url + 'register', JSON.stringify(details), this.getHeaders());
  }
  login(details){
    return this.http.post(config.base_url + 'login', JSON.stringify(details), this.getHeaders());
  }
  update(details, id){
    return this.http.put(config.base_url + `users/${id}`, JSON.stringify(details), this.getHeaders());
  }
}
