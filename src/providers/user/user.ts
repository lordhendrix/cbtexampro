import { Injectable } from '@angular/core';
import { SafeHttpProvider } from '../safehttp/safehttp';
import { config } from '../../config/config';
import { Headers, RequestOptions} from '@angular/http';
 

@Injectable()
export class UserProvider {

  constructor(public http: SafeHttpProvider) {
  }

  getHeaders() {
    let headers = new Headers();

    headers.append("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT");
    headers.append("Accept", "application/json");
    headers.append("Content-Type", "application/json");
    headers.append("Content-Type", "application/x-www-form-urlencoded");

    return new RequestOptions({ headers: headers });
  }

  getUserSubscriptions(user_id){
    return this.http.get(`${config.base_url}users/${user_id}/subscriptions`, this.getHeaders());
  }

  getMoreSubscriptions(url){
    return this.http.get(url, this.getHeaders());
  }

  checkWallet(id) {
    return this.http.get(`${config.base_url}users/${id}`, this.getHeaders())
  }

  confirmPaystackPayment(body){
    return this.http.post(`${config.base_url}verifypaystack`, JSON.stringify(body), this.getHeaders());
  }
  subscribe(body){
    return this.http.post(`${config.base_url}subscribe`, JSON.stringify(body), this.getHeaders());
  }

  getTransactions(id){
    return this.http.get(`${config.base_url}users/${id}/transactions`, this.getHeaders());
  }

  getMoreTransactions(url){
    return this.http.get(url, this.getHeaders());
  }
  getBanks(){
    return this.http.get(`${config.base_url}banks`, this.getHeaders());
  }
  withdraw(id, body){
    return this.http.post(`${config.base_url}users/${id}/wallet/withdraw`, body, this.getHeaders());
  }
}
