import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';



@Injectable()
export class SessionProvider {

  constructor(public storage: Storage) {}

  newUser(user){
   return this.storage.set('user', user);
  }

  getUserToken(){
    this.storage.get('user').then((val) => {
      return val.api_token;
    });
  }
  getUserId(): string{
    let id;
    this.storage.get('user').then((val) => {
      id = val.id;
    });
    return id;
  }

  getUser(){
    let user;
    this.storage.get('user').then((val) => {
      user = val;
    });
    return user;
  }

  saveUserEdit(edit){
    this.storage.get('user').then((val:any) => {
      val = edit
      this.storage.set('user', val);
    })
  }

  removeUser(){
    this.storage.remove('user');
  }
  welcomeView(){
    this.storage.set('viewed', '1');
  }


}
