import { Headers, RequestOptions } from '@angular/http';
import { config } from './../../config/config';
import { SafeHttpProvider } from './../safehttp/safehttp';
import { Injectable } from '@angular/core';



@Injectable()
export class DirectoryProvider {

  constructor(public http: SafeHttpProvider) {
  }
  getHeaders() {
    let headers = new Headers();
  
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
    return new RequestOptions({ headers:headers});
  
  }
  getSchoolFeatured(){
    return this.http.get(config.base_url + 'schools?tag=featured', this.getHeaders());
  }

  getTutorFeatured(){
    return this.http.get(config.base_url + 'tutors?tag=featured', this.getHeaders());
  }

  searchSchool(query){
    return this.http.get(config.base_url + `schools?search=${query}`, this.getHeaders());
  }
  searchTutor(query){
    return this.http.get(config.base_url + `tutors?search=${query}`, this.getHeaders());
  }

  getMoreSearchedSchool(url){
    return this.http.get(url, this.getHeaders());
  }
  getMoreSearchedTutor(url){
    return this.http.get(url, this.getHeaders());
  }
  
}
