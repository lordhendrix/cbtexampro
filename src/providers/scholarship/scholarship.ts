import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { SafeHttpProvider } from '../safehttp/safehttp';
import { config } from '../../config/config';


@Injectable()
export class ScholarshipProvider {

  constructor(public http: SafeHttpProvider) {
    console.log('Hello ScholarshipProvider Provider');
  }
  getHeaders() {
    let headers = new Headers();
  
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
    return new RequestOptions({ headers:headers});
  
  }

  getAvailableSchorlaship(){
    return this.http.get(config.base_url + 'scholarship/subjects', this.getHeaders());
  }

  getSchorlashipQuestions(slug){
    return this.http.get(config.base_url + `scholarship/subjects/${slug}/questions`);
  }

  getPastWinner() {
    return this.http.get(config.base_url + "scholarship/pastwinners", this.getHeaders());
  }
  getQcLeader(id) {
    return this.http.get(config.base_url + `leaderboard/${id}`, this.getHeaders());
  }
}
