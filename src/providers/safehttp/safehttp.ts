import { Http, RequestOptions } from '@angular/http';
import { ConnectivityProvider } from '../connectivity/connectivity';
import { Injectable } from '@angular/core';
import { ToastController} from "ionic-angular";
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';


@Injectable()
export class SafeHttpProvider {

  constructor(public http: Http, private networkService : ConnectivityProvider, private toastCtrl : ToastController) {
  }


  public get(url: string, options?: RequestOptions) {
    if (this.networkService.noConnection()) {
      this.toastCtrl.create({
        message: 'Check Your Connection!',
        duration: 3000,
        position: 'bottom'
      }).present();
    } 
    else { 
      return this.http.get(url, options).timeout(7000).map(response =>  response.json()); 
    }
  }

  public post(url: string, body:any ,options?: RequestOptions) {
    if (this.networkService.noConnection()) {
      this.toastCtrl.create({
        message: 'Check Your Connection!',
        duration: 3000,
        position: 'bottom'
      }).present();
    } 
    else { 
      return this.http.post(url,body ,options).map(response => response.json()); 
    }
  }
  public put(url: string, body:any ,options?: RequestOptions) {
    if (this.networkService.noConnection()) {
      this.toastCtrl.create({
        message: 'Check Your Connection!',
        duration: 3000,
        position: 'bottom'
      }).present();
    } 
    else { 
      return this.http.put(url,body ,options).map(response =>  response.json()); 
    }
  }

}
