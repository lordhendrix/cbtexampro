import { Injectable } from "@angular/core";
import { config } from "../../config/config";
import { SafeHttpProvider } from "../safehttp/safehttp";
import { Headers, RequestOptions } from "@angular/http";
import { Storage } from "@ionic/storage";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class PrepexamProvider {
  token: string;
  constructor(
    public http: SafeHttpProvider,
    public qhttp: HttpClient,
    private storage: Storage
  ) {
    this.storage.get("user").then(val => {
      if (val != null) {
        this.token = val.api_token;
      }
    });
  }
  getHeaders() {
    let headers = new Headers();

    headers.append("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT");
    headers.append("Accept", "application/json");
    headers.append("content-type", "application/json");

    return new RequestOptions({ headers: headers });
  }

  getCategories() {
    return this.http.get(
      config.base_url + `categories?api_token=${this.token}`,
      this.getHeaders()
    );
  }
  getGroup() {
    return this.http.get(config.base_url + `groups?api_token=${this.token}`);
  }
  getGroupExam(groupname) {
    return this.http.get(
      config.base_url + `groups/${groupname}`,
      this.getHeaders()
    );
  }
  getExamSubjects(exam) {
    return this.http.get(
      config.base_url + `categories/${exam}?api_token=${this.token}`,
      this.getHeaders()
    );
  } 
  getPostUtmeSchools(slug){
    return this.http.get(config.base_url + `categories/${slug}/schools`, this.getHeaders());
  }

  checkSubscription(user_id, category_id) {
    return this.http.get(config.base_url + `users/${user_id}/subscribed/${category_id}`, this.getHeaders());
  }
  getSubjectQuestions(subjectSlug) {
    return this.qhttp.get(
      config.base_url +
        `subjects/${subjectSlug}/questions?api_token=${this.token}`);
  }
  submitExam(body) {
    return this.http.post(
      config.base_url + `submitexam?api_token=${this.token}`,
      JSON.stringify(body),
      this.getHeaders()
    );
  }
}
