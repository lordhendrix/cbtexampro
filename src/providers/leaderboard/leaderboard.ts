import { config } from "./../../config/config";
import { RequestOptions, Headers } from "@angular/http";
import { Injectable } from "@angular/core";
import { SafeHttpProvider } from "../safehttp/safehttp";

@Injectable()
export class LeaderboardProvider {
  constructor(public http: SafeHttpProvider) {
  }
  getHeaders() {
    let headers = new Headers();

    headers.append("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT");
    headers.append("Accept", "application/json");
    headers.append("content-type", "application/json");

    return new RequestOptions({ headers: headers });
  }

  getLeaderboard() {
    return this.http.get(config.base_url + "leaderboard", this.getHeaders());
  }
}
