import { config } from './../../config/config';
import { Injectable } from '@angular/core';
import { SafeHttpProvider } from '../safehttp/safehttp';
import { RequestOptions, Headers } from '@angular/http';




@Injectable()
export class ElearningProvider {

  constructor(public http: SafeHttpProvider) {
  }
  
  getHeaders() {
    let headers = new Headers();

    headers.append("Access-Control-Allow-Origin", "*");
    headers.append("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT");
    headers.append("Accept", "application/json");
    headers.append("content-type", "application/json");

    return new RequestOptions({ headers: headers });
  }

  getSubjects(id){
    return this.http.get(`${config.base_url}document-categories/${id}/subjects?with=topics`, this.getHeaders());
  }

  getCategories(){
    return this.http.get(`${config.base_url}document-categories`, this.getHeaders());
  }
  getTopics(id){
    return this.http.get(`${config.base_url}document-categories/subjects/${id}/topics?with=documents`, this.getHeaders());
  } 

  getTopicDocuments(id){
    return this.http.get(`${config.base_url}document-categories/topics/${id}`);
  }
}
