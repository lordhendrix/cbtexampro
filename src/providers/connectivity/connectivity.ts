import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';
  


@Injectable()
export class ConnectivityProvider {

  onDevice: boolean;

  constructor( private network : Network, private platform: Platform) {
    this.onDevice  = this.platform.is('cordova');
  }

  // ---- if there isn't any connection
  noConnection(){
    return( this.network.type === 'none');
  }



  // ------ if device is offline ---
  isOffline(): boolean{
    if(this.onDevice && this.network.type){
      return this.network.type == 'none';
    }
    else{
      return !navigator.onLine;
    }
  }
    
}
