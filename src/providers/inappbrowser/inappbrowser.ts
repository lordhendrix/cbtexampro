import { Injectable } from "@angular/core";
import {
  ThemeableBrowser,
  ThemeableBrowserOptions,
  ThemeableBrowserObject
} 
from "@ionic-native/themeable-browser";
import { SpinnerDialog } from '@ionic-native/spinner-dialog';


@Injectable()
export class InappbrowserProvider {
  constructor(private themeableBrowser: ThemeableBrowser,private spinnerDialog: SpinnerDialog) {}

  options: ThemeableBrowserOptions = {
    statusbar: {
      color: "#016e1a"
    },
    toolbar: {
      height: 60,
      color: "#01c22e"
    },
    title: {
      color: "ffffff",
      showPageTitle: true,
    },
    backButton: {
      wwwImage: "",
      imagePressed: "back_pressed",
      align: "left",
      event: "backPressed"
    },
    forwardButton: {
      wwwImage: "",
      imagePressed: "forward_pressed",
      align: "left",
      event: "forwardPressed"
    },
    closeButton: {
      wwwImage: "assets/imgs/left-arrow.png",
      imagePressed: "close_pressed",
      align: "left",
      event: "closePressed"
    },
    customButtons: [
      {
        image: "share",
        imagePressed: "share_pressed",
        align: "right",
        event: "sharePressed"
      }
    ],
    backButtonCanClose: true
  };
  openBrowser(url) {
    const browser: ThemeableBrowserObject = this.themeableBrowser.create(url, '_blank', this.options);
    browser.on('loadstart').subscribe(event => {
      this.spinnerDialog.show('', '', true);
    })
    browser.on('loadstop').subscribe(event => {
      this.spinnerDialog.hide();
      browser.show();
    })
  }
}
