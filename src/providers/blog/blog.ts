import { RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { SafeHttpProvider } from '../safehttp/safehttp';
import { config } from '../../config/config';


@Injectable()
export class BlogProvider {

  constructor(public http: SafeHttpProvider) {
    console.log('Hello BlogProvider Provider');
  }

  getHeaders() {
    let headers = new Headers();
  
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
    return new RequestOptions({ headers:headers});
  
  }

  getAllArticles(){
    return this.http.get(config.base_url+'articles', this.getHeaders())
  }

  getMoreArticles(url){
    return this.http.get(url, this.getHeaders());
  }
}
