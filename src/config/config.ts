

let domain_name = "https://cbtexampro.com/";

export const config = {
    base_url : domain_name + "api/v1/",
    cloudinaryUrl : 'https://api.cloudinary.com/v1_1/pepproject/image/upload',
    resetPasswordUrl : domain_name + "password/reset",
    paystack: "pk_test_a3c6507e7a82c63308de9c5863bbe0950492d508",
    email: "support@cbtexampro.com",
    support_url :  domain_name + "api/",
    privary_policy: domain_name + "privacy-policy"
};
