import { Injectable, Pipe } from '@angular/core';
import * as moment from 'moment';

/*
  Generated class for the Timesago pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'timesago'
})
@Injectable()
export class Timesago {
  now:any;

  /*
    Takes a value and makes it lowercase.
   */
  transform(value, args) {
    this.now = moment(value).fromNow();
    return this.now;
  }
}