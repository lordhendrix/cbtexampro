import { NgModule } from '@angular/core';
import { KeysPipe } from './keys';
import { SafePipe } from './safe';


@NgModule({
  declarations: [
    KeysPipe,
    SafePipe
  ],
    exports: [
  KeysPipe,
  SafePipe
],
})
export class PipesModule {}
