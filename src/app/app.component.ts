import { Component, ViewChild } from "@angular/core";
import { Platform, App, AlertController, Events, Nav } from "ionic-angular";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { HeaderColor } from "@ionic-native/header-color";
// Menu Pages
import { ProfilePage } from "./../pages/profile/profile";
import { BlogPage } from "../pages/blog/blog";
import { TabsPage } from "../pages/tabs/tabs";
import { LeaderboardPage } from "../pages/leaderboard/leaderboard";
import { WalletPage } from "../pages/wallet/wallet";
import { PrepCenterPage } from "../pages/prep-center/prep-center";
import { ELearningPage } from "../pages/e-learning/e-learning";
import { SchooldirectoryPage } from "../pages/schooldirectory/schooldirectory";
import { Storage } from "@ionic/storage";

import { UserProvider } from "../providers/user/user";
import { InappbrowserProvider } from "../providers/inappbrowser/inappbrowser";
import { config } from "../config/config";
import { Quiz4cashPage } from "../pages/quiz4cash/quiz4cash";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  name: string;
  image: string;
  wallet: string;
  rootPage: any;
  pages: Array<{ title: string; component: any; icon: string }>;
  loggedIn: boolean;
  @ViewChild(Nav) nav: Nav;
  constructor(
    public platform: Platform,
    private headerColor: HeaderColor,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public storage: Storage,
    private app: App,
    private event: Events,
    private alertCtrl: AlertController,
    private userService: UserProvider,
    private inappBrowser: InappbrowserProvider
  ) {
    this.initializeApp();
    // ------- Events ---------- //
    this.event.subscribe("onLogin", () => {
      storage.get("user").then(val => {
        this.name = val.name;
        this.image = val.image;
        this.loggedIn = true;
        try{
          this.userService.checkWallet(val.id).subscribe((response: any) => {
            this.wallet = response.wallet_balance;
          });
        } catch(err){}
      });
    });
    this.event.subscribe("change_image", image => {
      this.image = image;
    });
    this.event.subscribe("onLogout", () => {
      this.loggedIn = false;
      this.image = null;
    });
    this.event.subscribe("onEdit", () => {
      storage.get("user").then(val => {
        this.name = val.name;
      });
    });
    this.event.subscribe("onWalletChange", () => {
      this.storage.get("user").then(val => {
        this.userService.checkWallet(val.id).subscribe((response: any) => {
          this.wallet = response.wallet_balance;
        });
      });
    });

    //------ End of Events ----- //
    this.pages = [
      { title: "Home", component: TabsPage, icon: "home" },
      { title: "Profile", component: ProfilePage, icon: "person" },
      { title: "Take Exam", component: PrepCenterPage, icon: "book" },
      { title: "Quiz4Cash", component: Quiz4cashPage, icon: "trophy"},
      { title: "Leaderboard", component: LeaderboardPage, icon: "open" },
      { title: "Find Centers", component: SchooldirectoryPage, icon: "search" },
      { title: "My Wallet", component: WalletPage, icon: "card" },
      { title: "News", component: BlogPage, icon: "paper" }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.keepSession();

      this.headerColor.tint("#016e1a");

      this.statusBar.backgroundColorByHexString("#016e1a");
      this.splashScreen.hide();

      this.platform.registerBackButtonAction(() => {
        const overlayView = this.app._appRoot._overlayPortal._views[0];
        if(overlayView && overlayView.dismiss) {
          overlayView.dismiss();
        } else {
          let nav = this.app.getActiveNav();
          if(nav.canGoBack()){
            nav.pop();
          } else {
            this.alertCtrl.create({
              subTitle: 'Do you want to quit?',
              buttons: [{
                text: 'cancel',
                role: 'Cancel'
              },
              {
                text:'ok',
                handler: () => {
                  this.platform.exitApp();
                }
              }]
            }).present();
          }
        }
      });
    });
  }

  public keepSession() {
    this.storage.get("user").then(val => {
      if (val != null) {
        this.name = val.name;
        this.image = val.image;
        try{
          this.userService.checkWallet(val.id).subscribe((response: any) => {
            console.log('here');
            this.wallet = response.wallet_balance;
          });
        } catch(err){}
        this.rootPage = TabsPage;
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
        this.storage.get("viewed").then(value => {
          if (value != null) {
            this.rootPage = TabsPage;
          } else {
            this.rootPage = "WelcomePage";
          }
        });
      }
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  logOut() {
    this.image = null;
    this.storage.remove("user");
    this.loggedIn = false;
    this.app.getRootNav().setRoot("WelcomePage");
  }

  openSupport() {
    this.storage.get('user').then(val => {
      this.inappBrowser.openBrowser(`${config.support_url}tickets?api_token=${val.api_token}&user_id=${val.id}`);
    })
  }
}
