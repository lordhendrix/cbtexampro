import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { CloudinaryModule, CloudinaryConfiguration, Cloudinary } from '@cloudinary/angular-5.x';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Keyboard } from '@ionic-native/keyboard';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { HeaderColor } from '@ionic-native/header-color';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { FileOpener } from '@ionic-native/file-opener';
import { SpinnerDialog } from '@ionic-native/spinner-dialog';
import { HttpClientModule } from '@angular/common/http';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Screenshot } from '@ionic-native/screenshot';


// Page Modules 
import { PrepCenterPageModule } from '../pages/prep-center/prep-center.module'
import { ELearningPageModule } from '../pages/e-learning/e-learning.module';
import { BlogPageModule } from '../pages/blog/blog.module';
import { TutordirectoryPageModule } from '../pages/tutordirectory/tutordirectory.module';
import { SchooldirectoryPageModule } from '../pages/schooldirectory/schooldirectory.module';
import { ProfilePageModule } from '../pages/profile/profile.module';
import { HomePageModule } from '../pages/home/home.module';
import { BloginfoPageModule } from '../pages/bloginfo/bloginfo.module';
import { EditprofilePageModule } from '../pages/editprofile/editprofile.module';
import { TabsPage } from '../pages/tabs/tabs';
import { Quiz4cashPageModule } from '../pages/quiz4cash/quiz4cash.module';
import { LeaderboardPageModule } from '../pages/leaderboard/leaderboard.module';
import { WalletPageModule } from '../pages/wallet/wallet.module';
import { LoginPageModule } from '../pages/login/login.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { QuizPageModule } from '../pages/quiz/quiz.module';
import { QuizresultPageModule } from '../pages/quizresult/quizresult.module';
import { WelcomePageModule } from '../pages/welcome/welcome.module';
import { PrepExamCategoriesPageModule } from '../pages/prep-exam-categories/prep-exam-categories.module';
import { TutorsProfilePageModule } from '../pages/tutors-profile/tutors-profile.module';
import { PrepexamtakingPageModule } from '../pages/prepexamtaking/prepexamtaking.module';
import { CalculatorPage } from './../pages/calculator/calculator';
import { HelpPageModule } from './../pages/help/help.module';
import { AboutUsPageModule } from './../pages/about-us/about-us.module';
import { ContactUsPageModule } from './../pages/contact-us/contact-us.module';
import { SchoolDirectoryInfoPageModule } from '../pages/school-directory-info/school-directory-info.module';
import { ScholarshipexamtakingPageModule } from '../pages/scholarshipexamtaking/scholarshipexamtaking.module';
import { MakePaymentPageModule } from '../pages/make-payment/make-payment.module';
import { DirectorySearchPageModule } from '../pages/directory-search/directory-search.module';
import { TutorSearchPageModule } from '../pages/tutor-search/tutor-search.module';
import { ElearningcategoriesPageModule } from '../pages/elearningcategories/elearningcategories.module';
import { ElearningsubjectsPageModule } from '../pages/elearningsubjects/elearningsubjects.module';
import { ElearningtopicsPageModule } from '../pages/elearningtopics/elearningtopics.module';
import { ElearningtopicPageModule } from '../pages/elearningtopic/elearningtopic.module';
import { ElearningDescriptionPageModule } from '../pages/elearning-description/elearning-description.module';
import { PostutmePageModule } from '../pages/postutme/postutme.module';
import { WithdrawalPageModule } from '../pages/withdrawal/withdrawal.module';


//Directives 
import { DirectivesModule } from '../directives/directives.module';

//Providers
import { SessionProvider } from '../providers/session/session';
import { PrepexamProvider } from '../providers/prepexam/prepexam';
import { ConnectivityProvider } from '../providers/connectivity/connectivity';
import { SafeHttpProvider } from '../providers/safehttp/safehttp';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { DirectoryProvider } from '../providers/directory/directory';
import { InappbrowserProvider } from '../providers/inappbrowser/inappbrowser';
import { BlogProvider } from '../providers/blog/blog';
import { LeaderboardProvider } from '../providers/leaderboard/leaderboard';
import { UserProvider } from '../providers/user/user';
import { ScholarshipProvider } from '../providers/scholarship/scholarship';
import { ElearningProvider } from '../providers/elearning/elearning';



@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    CalculatorPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    DirectivesModule,
    HelpPageModule,
    PrepCenterPageModule,
    ELearningPageModule,
    LeaderboardPageModule,
    BlogPageModule,
    BloginfoPageModule,
    EditprofilePageModule,
    ProfilePageModule,
    TutordirectoryPageModule,
    SchooldirectoryPageModule,
    PostutmePageModule,
    SchoolDirectoryInfoPageModule,
    WithdrawalPageModule,
    HomePageModule,
    WalletPageModule,
    Quiz4cashPageModule,
    LoginPageModule,
    ContactUsPageModule,
    AboutUsPageModule,
    QuizPageModule,
    QuizresultPageModule,
    SignupPageModule,
    WelcomePageModule,
    PrepExamCategoriesPageModule,
    TutorsProfilePageModule,
    PrepexamtakingPageModule,
    ScholarshipexamtakingPageModule,
    MakePaymentPageModule,
    DirectorySearchPageModule,
    TutorSearchPageModule,
    ElearningcategoriesPageModule,
    ElearningsubjectsPageModule,
    ElearningtopicsPageModule,
    ElearningtopicPageModule,
    ElearningDescriptionPageModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    CloudinaryModule.forRoot({Cloudinary}, { cloud_name : 'pepproject'} as CloudinaryConfiguration)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    CalculatorPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    SpinnerDialog,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SessionProvider,
    PrepexamProvider,
    ConnectivityProvider,
    SafeHttpProvider,
    AuthenticationProvider,
    SocialSharing,
    SessionProvider,
    Screenshot,
    Camera,
    File,
    FileOpener, 
    HeaderColor,
    FileTransfer,
    FilePath,
    ThemeableBrowser,
    DirectoryProvider,
    InappbrowserProvider,
    BlogProvider,
    ScholarshipProvider,
    LeaderboardProvider,
    NativePageTransitions,
    UserProvider,
    ElearningProvider
  ]
})
export class AppModule {}
