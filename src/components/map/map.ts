import { Component, Input, ViewChild, ElementRef , OnInit} from '@angular/core';

declare var google: any;

@Component({
  selector: 'map',
  templateUrl: 'map.html'
})
export class MapComponent implements OnInit {
  @Input('longitude') longitude: string;
  @Input('latitude') latitude: string;

  @ViewChild('map') mapElement: ElementRef;
  public map: any;

  constructor() {
  }

  ngOnInit(){
    this.createMap()
  }
  createMap(){
    const location = new google.maps.LatLng(this.latitude, this.longitude);
    let mapOptions = {
      center: location,
      zoom : 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let myLatlng = new google.maps.LatLng(this.latitude, this.longitude);
    let marker = new google.maps.Marker({
      position: myLatlng ,
      map: this.map,
    });
  }

}
