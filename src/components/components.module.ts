import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular'
import { MapComponent } from './map/map';
import { UserLoginComponent } from './user-login/user-login';

@NgModule({
	declarations: [
    MapComponent,
    UserLoginComponent],
	imports: [IonicModule],
	exports: [
    MapComponent,
    UserLoginComponent]
})
export class ComponentsModule {}
