import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';


@Component({
  selector: 'user-login',
  templateUrl: 'user-login.html'
})
export class UserLoginComponent {

  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 400,
    slowdownfactor: -1,
    iosdelay: 50,
    androiddelay: 50,
  }
  constructor(private navCtrl: NavController, private transition: NativePageTransitions) {
  }

  login(){
    this.transition.slide(this.options);
    this.navCtrl.push('LoginPage');
  }
  signup(){
    this.transition.slide(this.options);
    this.navCtrl.push('SignupPage');
  }
}
