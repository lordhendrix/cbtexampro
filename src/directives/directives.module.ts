import { NgModule } from '@angular/core';
import { SwipeSegmentDirective } from './swipeable-segment';
import { SwipeTabDirective } from './swipeable-tabs';


@NgModule({
    declarations: [
        SwipeSegmentDirective,
        SwipeTabDirective
    ],
    exports:[
        SwipeSegmentDirective,
        SwipeTabDirective
    ]
})
export class DirectivesModule{}