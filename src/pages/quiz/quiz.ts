import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController,
  MenuController,
  ToastController
} from "ionic-angular";
import { PrepexamProvider } from "../../providers/prepexam/prepexam";
import { Storage } from "@ionic/storage";
import { CalculatorPage } from "./../calculator/calculator";
import { ScholarshipProvider } from "../../providers/scholarship/scholarship";

@IonicPage()
@Component({
  selector: "page-quiz",
  templateUrl: "quiz.html"
})
export class QuizPage {
  user_id: string;
  examName: string;
  questions: any[] = [];
  loaded: boolean = false;
  time: any;
  realtime: string;
  selectedQuestion: any[] = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private prepExam: PrepexamProvider,
    private scholar: ScholarshipProvider,
    private modalCtrl: ModalController,
    private storage: Storage,
    private menu: MenuController,
    private loader: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController
  ) {
    let examContent = this.navParams.get("examContent");
    let examtype = this.navParams.get("prep");
    this.storage.get("user").then(val => {
      this.user_id = val.id;
    });
    this.examName = examContent.name;
    this.realtime = examContent.duration;
    if (examtype == "exam") {
      this.getQuestions(examContent.slug);
    } else {
      this.getScholarQuestions(examContent.slug);
    }
  }

  ionViewDidLoad() {
    this.menu.swipeEnable(false);
  }
  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  openCalc() {
    this.modalCtrl.create(CalculatorPage).present();
  }

  getQuestions(slug) {
    let loading = this.loader.create({
      content: "Fetching Questions"
    });
    loading.present();
    this.prepExam
      .getSubjectQuestions(slug)
      .subscribe((response: any) => {
        this.questions = response;
        this.populateSelected(response);
        this.loaded = true;
        this.calculateTime(this.realtime);
        loading.dismiss();
      }, (err) => {
        loading.dismiss()
        this.toastCtrl.create({
          message: 'Internet connection is slow!',
          duration: 3000,
          position: 'bottom'
        }).present();
        if(this.questions.length == 0){
          this.getQuestions(slug);
        }
      });
  }
  identify(index, club) {
    return club.id;
  }
  getScholarQuestions(slug) {
    let loading = this.loader.create({
      content: "Fething Questions"
    });
    loading.present();
    this.scholar
      .getSchorlashipQuestions(slug)
      .subscribe(response => {
        this.questions = response;
        this.populateSelected(response);
        this.loaded = true;
        this.calculateTime(this.realtime);
        loading.dismiss();
      }, (err) => {
        this.toastCtrl.create({
          message: 'Internet connection is slow!',
          duration: 3000,
          position: 'bottom'
        }).present();
      });
  }

  populateSelected(questions) {
    for (let i = 0; i < questions.length; i++) {
      this.selectedQuestion.push({
        questionId: questions[i].id,
        answerId: null
      });
    }
  }
  calculateTime(time) {
    let minute = time;
    let second = 0;
    let timer = setInterval(() => {
      if (minute == 0 && second == 0) {
        clearInterval(timer);
        this.getResults();
      } else if (second == 0) {
        minute--;
        second = 59;
      } else {
        second = second - 1;
      }
      this.time = minute + ":" + second;
    }, 1000);
  }

  submit() {
    if (this.time != 0) {
      this.alertCtrl
        .create({
          subTitle: "Are you ready to Submit ?",
          buttons: [
            {
              text: "Cancel",
              role: "cancel"
            },
            {
              text: "Proceed",
              handler: () => {
                this.getResults();
              }
            }
          ]
        })
        .present();
    }
  }

  getResults() {
    let loading = this.loader.create({
      content: "Submitting ..."
    });
    loading.present();
    let body = {
      answers: this.selectedQuestion,
      session_id: this.guidGenerator(),
      userId: this.user_id
    };
    this.prepExam
      .submitExam(body).subscribe(response => {
        loading.dismiss();
        this.navCtrl.setRoot("QuizresultPage", {
          result: response
        });
      }, (err) => {
        this.toastCtrl.create({
          message: 'Check Your Connection!',
          duration: 3000,
          position: 'bottom'
        }).present();
      });
  }
  addToSelected(question, answer) {
    for (let i = 0; i < this.selectedQuestion.length; i++) {
      if (this.selectedQuestion[i].questionId == question) {
        this.selectedQuestion[i].answerId = answer;
        break;
      }
    }
  }

  guidGenerator() {
    var S4 = function() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
    return (
      S4() +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      "-" +
      S4() +
      S4() +
      S4()
    );
  }
}
