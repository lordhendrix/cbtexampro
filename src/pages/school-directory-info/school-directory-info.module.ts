import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchoolDirectoryInfoPage } from './school-directory-info';
import { ComponentsModule } from '../../components/components.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [
    SchoolDirectoryInfoPage,
  ],
  imports: [
    ComponentsModule,
    LazyLoadImageModule,
    IonicPageModule.forChild(SchoolDirectoryInfoPage),
  ],
})
export class SchoolDirectoryInfoPageModule {}
