import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SchoolDirectoryInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-school-directory-info',
  templateUrl: 'school-directory-info.html',
})
export class SchoolDirectoryInfoPage {
  school: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.school = this.navParams.get('school');
  }

  ionViewDidLoad() {
   
  }

}
