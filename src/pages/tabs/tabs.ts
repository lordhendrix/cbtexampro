import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { BlogPage } from '../blog/blog';
import { Tabs } from 'ionic-angular';
import { SwipeTabDirective } from '../../directives/swipeable-tabs';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  rootOne = HomePage;
  rootTwo = ProfilePage;
  rootThree = BlogPage;

  @ViewChild(SwipeTabDirective) swipeTabDirective: SwipeTabDirective;
  @ViewChild('myTabs') tabRef: Tabs;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {}

  transition($event) {
    this.swipeTabDirective.onTabInitialized($event.index);
  }

  onTabChange(index: number) {
    this.tabRef.select(index);
  }

}
