import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Quiz4cashPage } from './quiz4cash';
import { DirectivesModule } from '../../directives/directives.module';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    Quiz4cashPage,
  ],
  imports: [
    DirectivesModule,
    ComponentsModule,
    IonicPageModule.forChild(Quiz4cashPage),
  ],
})
export class Quiz4cashPageModule {}
