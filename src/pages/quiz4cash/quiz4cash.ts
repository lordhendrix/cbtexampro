import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController
} from "ionic-angular";
import { ScholarshipProvider } from "../../providers/scholarship/scholarship";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: "page-quiz4cash",
  templateUrl: "quiz4cash.html"
})
export class Quiz4cashPage {
  current: string = "quizs";
  selected: string = "daily";
  tabs: Array<string> = ["quizs", "leaderboard", "pastwinner"];
  subjects: any[];
  nextWinners: string;
  loggedIn;
  searched: boolean;
  winners: any[];
  overall: any[];
  daily: any[];
  monthly: any[];
  id;
  constructor(
    public navCtrl: NavController,
    private modalCtrl: ModalController,
    public navParams: NavParams,
    private provider: ScholarshipProvider,
    private loader: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {}

  ionViewWillEnter() {
    this.storage.get("user").then(val => {
      if (val != null) {
        this.getSubject();
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
  }
  onTabChanged(tabName) {
    this.current = tabName;
  }

  getSubject() {
    let loading = this.loader.create({
      content: "Loading..."
    });
    loading.present();
    this.provider.getAvailableSchorlaship().subscribe(
      response => {
        this.subjects = response;
        console.log(this.subjects);
        this.getPastWinners();
        loading.dismiss();
      },
      err => {
        loading.dismiss();
        this.alertCtrl
          .create({
            subTitle: "Internet Connection is slow",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  loading.dismiss();
                  this.navCtrl.pop();
                }
              }
            ]
          })
          .present();
      }
    );
  }


  getQcLeaderboard(id) {
    this.searched = false;
    let loading = this.loader.create({
      content: 'Loading'
    });
    loading.present();
    this.provider.getQcLeader(id).subscribe((response: any) => {
      //console.log(response);
      this.overall = response.overall;
      this.monthly = response.monthly;
      this.daily = response.daily;
      this.searched = true;
      loading.dismiss();
    }, (err) => {
      loading.dismiss();
    });
  }
  getPastWinners(){
    this.provider.getPastWinner().subscribe((response: any) => {
      this.winners = response.data;
      this.nextWinners = response.next_page_url;
    })
  }
  openExam(content) {
    let modal = this.modalCtrl.create("ScholarshipexamtakingPage", {
      examContent: content
    });
    modal.present();
  }
}
