import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App,ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-scholarshipexamtaking',
  templateUrl: 'scholarshipexamtaking.html',
})
export class ScholarshipexamtakingPage {
  examContent: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private app : App, private viewCtrl: ViewController) {
    this.examContent = this.navParams.get('examContent');
  }

  ionViewDidLoad() {
  }
  takeExam(){
    let alert = this.alertCtrl.create({
      subTitle: 'Are you ready to take exam?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Proceed',
          handler: () => {

            this.app.getRootNav().setRoot('QuizPage', {
              examContent: this.examContent, prep : 'scholarship'
            }).then( () => {
              const index = this.viewCtrl.index;
              this.navCtrl.remove(index);
            });
          }
        }
      ]
    })
    alert.present();
  }

}
