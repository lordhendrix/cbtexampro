import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScholarshipexamtakingPage } from './scholarshipexamtaking';

@NgModule({
  declarations: [
    ScholarshipexamtakingPage,
  ],
  imports: [
    IonicPageModule.forChild(ScholarshipexamtakingPage),
  ],
})
export class ScholarshipexamtakingPageModule {}
