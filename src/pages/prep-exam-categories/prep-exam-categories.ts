import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { PrepexamProvider } from "../../providers/prepexam/prepexam";

@IonicPage()
@Component({
  selector: "page-prep-exam-categories",
  templateUrl: "prep-exam-categories.html"
})
export class PrepExamCategoriesPage {
  pageName: string;
  loaded: Promise<boolean> = Promise.resolve(false);
  exams: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private prepExam: PrepexamProvider,
    private loading: LoadingController,
    private alertCtrl: AlertController
  ) {
    let page = this.navParams.get("page");
    this.pageName = page;
    this.getExamFromGroup(page);
  }

  ionViewDidLoad() {}

  gotoExamSelectionPage(exam) {
    if(exam.slug != "post-utme"){
      this.navCtrl.push("PrepexamtakingPage", {
        exam: exam
      });
    } else{
      this.navCtrl.push("PostutmePage", {
        exam: exam
      })
    }
  }
  getExamFromGroup(group) {
    let loader = this.loading.create({
      content: "Loading..."
    });
    loader.present();
    this.prepExam.getGroupExam(group).subscribe(
      response => {
        // console.log(response);
        this.exams = response.data;
        this.loaded = Promise.resolve(true);
        loader.dismiss();
      },
      err => {
        this.alertCtrl
          .create({
            subTitle: "Internet Connection is slow",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  loader.dismiss();
                  this.navCtrl.pop();
                }
              }
            ]
          })
          .present();
      }
    );
  }
}
