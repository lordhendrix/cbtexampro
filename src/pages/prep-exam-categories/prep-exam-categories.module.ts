import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrepExamCategoriesPage } from './prep-exam-categories';

@NgModule({
  declarations: [
    PrepExamCategoriesPage,
  ],
  imports: [
    IonicPageModule.forChild(PrepExamCategoriesPage),
  ],
})
export class PrepExamCategoriesPageModule {}
