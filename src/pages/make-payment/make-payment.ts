import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ViewController,
  AlertController,
  LoadingController,
  Events
} from "ionic-angular";
import { config } from "../../config/config";
import { UserProvider } from "../../providers/user/user";
import { Storage } from "@ionic/storage";

declare var PaystackPop;

@IonicPage()
@Component({
  selector: "page-make-payment",
  templateUrl: "make-payment.html"
})
export class MakePaymentPage {
  price ;
  category;
  id;
  payfromwallet: boolean = false;
  showcardInput: boolean = false;
  loaded: Promise<boolean>;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private userService: UserProvider,
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private event: Events,
    private viewController: ViewController
  ) {
    let prevPage = this.navParams.get("from");
    if (prevPage == "wallet") {
      this.storage.get('user').then(val => {
        this.id = val.id;
        this.showcardInput = true;
        this.loaded = Promise.resolve(true);
      });
    } else {
      this.category = this.navParams.get("category");
      this.price = this.category.price;
      this.storage.get("user").then(val => {
        this.id = val.id;
        if (val != null) {
          this.userService.checkWallet(val.id).subscribe((response: any) => {
            const wallet = response.wallet_balance;
            if (this.price <= wallet) {
              this.payfromwallet = true;
            } else {
              this.payfromwallet = false;
            }
            this.loaded = Promise.resolve(true);
          });
        }
      });
    }
  }

  ionViewDidLoad() {}

  deductFromWallet() {
    let body = {
      user_id: this.id,
      category_id: this.category.id
    };
    this.userService.subscribe(body).subscribe((response: any) => {
      this.viewController.dismiss({ status: "success" });
      this.event.publish("onWalletChange");
    });
  }

  payWithPaystack() {
    var handler = PaystackPop.setup({
      key: config.paystack,
      email: config.email,
      amount: this.price * 100,
      ref: "" + Math.floor(Math.random() * 1000000000 + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
      callback: response => {
        if (response.status == "success") {
          let body = {
            user_id: this.id,
            transaction_ref: response.reference
          };
          let loading = this.loadingCtrl.create({
            content: 'Subscribing...'
          })
          loading.present();
          this.userService.confirmPaystackPayment(body).subscribe(
            (response: any) => {
              let categorySubscription = {
                user_id: this.id,
                category_id: this.category.id
              };
              this.userService
                .subscribe(categorySubscription)
                .subscribe((response: any) => {
                  loading.dismiss();
                  this.alertCtrl.create({
                    subTitle: "Payment Successful",
                    buttons: [
                      {
                        text: "ok",
                        handler: () => {
                          this.viewController.dismiss({ status: "success" });
                          this.event.publish("onWalletChange");
                        }
                      }
                    ]
                  }).present();
                }, (err) => {
                  loading.dismiss();
                  console.log(err);
                });
            },
            err => {
              loading.dismiss();
              console.log(err);
            }
          );
        }
      },
      onClose: () => {
        this.alertCtrl
          .create({
            subTitle: "Transaction unsuccessful!",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  this.viewController.dismiss({ status: "error" });
                }
              }
            ]
          })
          .present();
      }
    });
    handler.openIframe();
  }

  topUpWalletWithPaystack() {
    var handler = PaystackPop.setup({
      key: config.paystack,
      email: config.email,
      amount: this.price * 100,
      ref: "" + Math.floor(Math.random() * 1000000000 + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
      callback: response => {
        if (response.status == "success") {
          let body = {
            user_id: this.id,
            transaction_ref: response.reference
          };
          this.userService.confirmPaystackPayment(body).subscribe(
            (response: any) => {
              this.alertCtrl.create({
                subTitle: "Payment Successful",
                buttons: [
                  {
                    text: "ok",
                    handler: () => {
                      this.viewController.dismiss({ status: "success" });
                      this.event.publish("onWalletChange");
                    }
                  }
                ]
              }).present();
            },
            err => {
              console.log(err);
            }
          );
        }
      },
      onClose: () => {
        this.alertCtrl
          .create({
            subTitle: "Transaction unsuccessful!",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  this.viewController.dismiss({ status: "error" });
                }
              }
            ]
          })
          .present();
      }
    });
    handler.openIframe();
  }

  onInputChange(newValue){
    this.price = newValue.value
  }



}
