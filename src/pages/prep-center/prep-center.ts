import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { PrepexamProvider } from "../../providers/prepexam/prepexam";

@IonicPage()
@Component({
  selector: "page-prep-center",
  templateUrl: "prep-center.html"
})
export class PrepCenterPage {
  categories: any;
  categoryKey = Object.keys;
  categoriesKey;
  KeyLength;
  loaded: Promise<boolean> = Promise.resolve(false);
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _prepExam: PrepexamProvider,
    private loader: LoadingController,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    this.getCategories();
  }

  openCategories(categoryPage: string) {
    this.navCtrl.push("PrepExamCategoriesPage", {
      page: categoryPage
    });
  }
  getCategories() {
    let loading = this.loader.create({
      content: "Loading..."
    });
    loading.present();
    this._prepExam.getGroup().subscribe(
      (response: any) => {
        this.categories = response;
        this.categoriesKey = Object.keys(this.categories);
        this.KeyLength = this.categoriesKey.length;
        this.loaded = Promise.resolve(true);
        loading.dismiss();
      },
      err => {
        this.alertCtrl
          .create({
            subTitle: "Internet Connection is slow",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  loading.dismiss();
                  this.navCtrl.pop();
                }
              }
            ]
          })
          .present();
      }
    );
  }
}
