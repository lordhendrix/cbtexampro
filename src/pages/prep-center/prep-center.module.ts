import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrepCenterPage } from './prep-center';
import { PipesModule } from '../../pipes/pipes.module';




@NgModule({
  declarations: [
    PrepCenterPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(PrepCenterPage),
  ],
})
export class PrepCenterPageModule {}
