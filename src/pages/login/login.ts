import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  LoadingController,
  AlertController,
  Events
} from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { SessionProvider } from "../../providers/session/session";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Keyboard } from "@ionic-native/keyboard";
import { config } from "./../../config/config";
import { InappbrowserProvider } from "./../../providers/inappbrowser/inappbrowser";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  private details: FormGroup;
  emailRegEx = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  passwordType: string = "password";
  passwordIcon: string = "eye-off";
  showFooter = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private auth: AuthenticationProvider,
    private session: SessionProvider,
    private loader: LoadingController,
    private alert: AlertController,
    private formBuilder: FormBuilder,
    private keyboard: Keyboard,
    private event: Events,
    private appBrowser: InappbrowserProvider
  ) {
    this.details = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
    this.keyboard.onKeyboardShow().subscribe(() => {
      this.showFooter = false;
    });
    this.keyboard.onKeyboardHide().subscribe(() => {
      this.showFooter = true;
    });
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }
  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  openSignUp() {
    this.navCtrl.push("SignupPage");
  }

  trimValue(formControl) {
    formControl.setValue(formControl.value.trim());
  }

  openForgotPassword() {
    this.appBrowser.openBrowser(config.resetPasswordUrl);
  }

  login() {
    let loading = this.loader.create({
      content: "Logging In..."
    });
    loading.present();
    this.auth.login(this.details.value).subscribe(
      (response: any) => {
        this.session.newUser(response.data).then(() => {
          this.event.publish("onLogin");
        });
        loading.dismiss();
        this.navCtrl.pop();
      },
      err => {
        let error = err.json().message;
        this.showAlert("Oops!", error);
        loading.dismiss();
      }
    );
  }

  showAlert(title: string, message: string) {
    let alert = this.alert.create({
      title: title,
      subTitle: message,
      buttons: ["ok"]
    });
    alert.present();
  }
}
