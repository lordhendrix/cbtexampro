import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElearningtopicsPage } from './elearningtopics';

@NgModule({
  declarations: [
    ElearningtopicsPage,
  ],
  imports: [
    IonicPageModule.forChild(ElearningtopicsPage),
  ],
})
export class ElearningtopicsPageModule {}
