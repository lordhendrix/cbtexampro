import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ViewController
} from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: "page-withdrawal",
  templateUrl: "withdrawal.html"
})
export class WithdrawalPage {
  price: number;
  id: number;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private userProvider: UserProvider,
    private alertCtrl: AlertController,
    private storage: Storage,
    private view: ViewController
  ) {
    this.storage.get("user").then(val => {
      if (val != null) {
        this.id = val.id;
      }
    });
  }

  ionViewDidLoad() {}

  withdraw() {
    let loading = this.loadingCtrl.create({
      content: "Withdrawing..."
    });

    loading.present();
    const body = {
      amount: this.price
    };
    this.userProvider.withdraw(this.id, body).subscribe(
      (response: any) => {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: response.status,
          subTitle: response.message,
          buttons: [{
            text: 'ok',
            role: 'cancel',
            handler: () => {
              this.view.dismiss();
            }
          }]
        });
        alert.present();
      },
      err => {
        loading.dismiss();
        let errMessage = err.json();
        try {
          let alert = this.alertCtrl.create({
            title: errMessage.error.code,
            subTitle: errMessage.error.message,
            buttons: ["ok"]
          });
          alert.present();
        } catch (e) {}
      }
    );
  }
}
