import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { LeaderboardProvider } from '../../providers/leaderboard/leaderboard';


@IonicPage()
@Component({
  selector: 'page-leaderboard',
  templateUrl: 'leaderboard.html',
})
export class LeaderboardPage {
  toggle = false;
  location: string;
  time: string;
  subject: string;
  leaderBoard: any[] = [];
  overall: any[];
  loaded: Promise<boolean>;
  loggedIn: boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,private leaderboard: LeaderboardProvider, private loader: LoadingController) {
  }

  ionViewWillEnter() {
      this.storage.get('user').then(val => {
        if(val != null){
          this.loggedIn = true;
          this.getLeaderboard();
        } else{
          this.loggedIn = false;
        }
      })
    }

    getLeaderboard(){
      let loading =  this.loader.create({
        content: "Loading..."
      });
      loading.present()
      this.leaderboard.getLeaderboard().subscribe((response) => {
        this.leaderBoard = response.overall;
        this.overall = response;
        this.loaded = Promise.resolve(true);
        loading.dismiss();
      })
    }
}
