import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController,
  Events
} from "ionic-angular";
import { UserProvider } from "../../providers/user/user";
import { Storage } from "@ionic/storage";


@IonicPage()
@Component({
  selector: "page-wallet",
  templateUrl: "wallet.html"
})
export class WalletPage {
  currentPage: string = "transactions";
  tabs: Array<string> = ["transactions", "purchase"];
  wallet: number;
  transactions: any[];
  loaded: Promise<boolean>;
  id;
  loggedIn: boolean;
  next_url: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private userService: UserProvider, 
    private storage: Storage,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private event: Events
  ) {
    this.event.subscribe('onWalletChange', () => {
      this.userService.checkWallet(this.id).subscribe((response: any) => {
        this.wallet = response.wallet_balance;
      })
    })
  }

  onTabChanged(tabOne) {
    this.currentPage = tabOne;
  }

  ionViewWillEnter() {
    this.storage.get("user").then(val => {
      if (val != null) {
        this.loggedIn = true;
        this.id = val.id;
        this.getWalletBalance(val.id);
      } else{
        this.loggedIn = false;
      }
    });
  }

  openWithdrawModal() {
    let modal  = this.modalCtrl.create('WithdrawalPage');
    modal.present();
  }
  formatDate(obj) {
    return new Date(obj.replace(/-/g, "/"));
  }

  getWalletBalance(id) {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });

    loader.present();
    this.userService.checkWallet(id).subscribe(
      (response: any) => {
        this.wallet = response.wallet_balance;
        this.userService.getTransactions(id).subscribe((response: any) => {
          this.next_url = response.next_page_url;
          this.transactions = response.data;
          loader.dismiss();
          this.loaded = Promise.resolve(true);
        }, (err) => {
          loader.dismiss();
        })
      },
      err => {
        loader.dismiss();
        this.alertCtrl.create({
          subTitle: "Network connection is slow!",
          buttons: [
            {
              text: "ok",
              handler: () => {
                this.navCtrl.pop();
              }
            }
          ]
        }).present();
      }
    );
  }

  showBankDetails() {
    this.alertCtrl.create({
      subTitle: 'Bank Details',
      message: 'Account Number : 0168611884, <br>  Account Name : Afrinovation International Ltd, <br> Bank Name: GTbank, <br> Email : payment@cbtexampro.com, <br> Phone Number: +234 8180094315 ',
      buttons: ['ok']
    }).present();
  }
  debitCard() {
    let modal = this.modalCtrl.create("MakePaymentPage", {
      from: "wallet"
    });

    modal.onDidDismiss(data => {
      if (data != null) {
        if (data.status == "success") {
          this.alertCtrl
            .create({
              subTitle: "Top-up successful",
              buttons: ["ok"]
            })
            .present();
        } else {
          this.alertCtrl
            .create({
              subTitle: "Top-up unsuccessful",
              buttons: ["ok"]
            })
            .present();
        }
         }
    });

    modal.present();
  }

  doInfinite(event){
    if(this.next_url != null){
      this.userService.getMoreTransactions(this.next_url).subscribe((response) => {
        this.next_url = response.next_page_url;
        response.data.forEach(element => {
          this.transactions.push(element);
        });
        event.complete();
      })
    } else{
      event.complete();
    }
  }

}
