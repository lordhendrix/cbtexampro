import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorsProfilePage } from './tutors-profile';
import { LazyLoadImageModule } from 'ng-lazyload-image';

@NgModule({
  declarations: [
    TutorsProfilePage,
  ],
  imports: [
    LazyLoadImageModule,
    IonicPageModule.forChild(TutorsProfilePage),
  ],
})
export class TutorsProfilePageModule {}
