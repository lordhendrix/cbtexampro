import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TutorsProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tutors-profile',
  templateUrl: 'tutors-profile.html',
})
export class TutorsProfilePage {
  tutor: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tutor = this.navParams.get('tutor');
    console.log(this.tutor);
  }

  ionViewDidLoad() {}

}
