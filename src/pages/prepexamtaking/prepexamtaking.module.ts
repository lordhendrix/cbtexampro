import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrepexamtakingPage } from './prepexamtaking';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  declarations: [
    PrepexamtakingPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(PrepexamtakingPage),
  ],
})
export class PrepexamtakingPageModule {}
