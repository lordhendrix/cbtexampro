import { Storage } from "@ionic/storage";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  ModalController,
  App
} from "ionic-angular";
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { PrepexamProvider } from "../../providers/prepexam/prepexam";

@IonicPage()
@Component({
  selector: "page-prepexamtaking",
  templateUrl: "prepexamtaking.html"
})
export class PrepexamtakingPage {
  pageName: string;
  loaded: boolean = false;
  subjects: any[] = [];
  selectedSubject: any = {};
  selectedSubjectSlug: string;
  hasSelected: boolean = false;
  loggedIn: boolean = true;
  category: any;
  options: NativeTransitionOptions = {
    direction: 'left',
    duration: 400,
    slowdownfactor: -1,
    iosdelay: 50,
    androiddelay: 50,
   };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private prepExam: PrepexamProvider,
    private loader: LoadingController,
    private app: App,
    private alertCtrl: AlertController,
    private storage: Storage,
    private modalCtrl: ModalController,
    private nativePageTransition: NativePageTransitions
  ) {}

  ionViewWillEnter() {
    let page = this.navParams.get("exam");
    this.category = page;
    this.pageName = page.name;
    this.storage.get("user").then(val => {
      if (val != null) {
        this.loggedIn = true;
        this.getExamSubjects(page.slug);
      } else {
        this.loggedIn = false;
      }
    });
  }

  getExamSubjects(exam_slug) {
    let loading = this.loader.create({
      content: "Loading..."
    });
    loading.present();
    this.prepExam.getExamSubjects(exam_slug).subscribe(
      (response: any) => {
        this.subjects = response[0].subjects;
        this.loaded = true;
        loading.dismiss();
      },
      err => {
        this.alertCtrl
          .create({
            subTitle: "Internet Connection is slow",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  loading.dismiss();
                  this.navCtrl.pop( );
                }
              }
            ]
          })
          .present();
      }
    );
  }
  onSelectSubject() {
    if (this.selectedSubjectSlug != null) {
      for (let i = 0; i < this.subjects.length; i++) {
        if (this.subjects[i].slug == this.selectedSubjectSlug) {
          this.selectedSubject = this.subjects[i];
          this.hasSelected = true;
          break;
        }
      }
    } else {
      this.hasSelected = false;
    }
  }
  checkSub(user_id, category) {
    this.prepExam.checkSubscription(user_id, category.id).subscribe(
      (response: any) => {
        this.nativePageTransition.slide(this.options);
        this.app.getRootNav().setRoot("QuizPage", {
          examContent: this.selectedSubject,
          prep: "exam"
        });
      },
      (err) => {
        let showPaymentAlert = this.alertCtrl.create({
          subTitle: `A annual subscription of NGN${category.price} is required to take this exam`,
          buttons: [
            {
              text: "cancel",
              role: "Cancel"
            },
            {
              text: 'Proceed',
              handler: () => {
                let modal = this.modalCtrl.create('MakePaymentPage', {
                  category: category,
                  from: 'exam',
                })
                modal.onDidDismiss(data => {
                 try{
                  if(data.status == 'success'){
                    this.nativePageTransition.slide(this.options);
                    this.app.getRootNav().setRoot("QuizPage", {
                      examContent: this.selectedSubject,
                      prep: "exam"
                    });
                   }
                   else{
                     this.alertCtrl.create({
                       subTitle: 'Subscription unsuccessful',
                       buttons: ['ok']
                     })
                   }
                 } catch(e){}
                })
                modal.present();
              }
            }
          ]
        });
        showPaymentAlert.present();
      }
    );
  }

  takeExam() {
    this.alertCtrl
      .create({
        title: "Take Exam",
        subTitle: "Are you ready to start exam?",

        buttons: [
          {
            text: "Cancel",
            role: "cancel"
          },
          {
            text: "Proceed",
            handler: () => {
              this.storage.get("user").then(val => {
                if (val != null) {
                  this.checkSub(val.id, this.category);
                }
              });
            }
          }
        ]
      })
      .present();
  }
}
