import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController,
  AlertController
} from "ionic-angular";
import { Storage } from "@ionic/storage";
import { Events } from "ionic-angular";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { config } from "../../config/config";
import {
  FileTransfer,
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { normalizeURL } from "ionic-angular";
import { UserProvider } from "../../providers/user/user";


declare var cordova;

@IonicPage()
@Component({
  selector: "page-editprofile",
  templateUrl: "editprofile.html"
})
export class EditprofilePage {
  user = {};
  picture_url: string;
  lastImage: string;
  id: string;
  loggedIn;
  banks: any[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private camera: Camera,
    private transfer: FileTransfer,
    private _loader: LoadingController,
    private _storage: Storage,
    private _sendData: AuthenticationProvider,
    private events: Events,
    private toastCtrl: ToastController,
    private alert: AlertController,
    private editService: AuthenticationProvider,
    private userProvider: UserProvider
  ) {}

  ionViewDidLoad() {
    this._storage.get("user").then(val => {
      if (val != null) {
        this.loggedIn = true;
        this.getBanks();
        this.user = val;
        (this.picture_url = val.image), (this.id = val.id);
      }else {
        this.loggedIn = false;
      }
    }); 
  }

  chooseImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetHeight: 200,
      targetWidth: 200
    };

    this.camera.getPicture(options).then(imageData => {
      this.lastImage = imageData;
      this.uploadImage();
    });
  }

  uploadImage() {
    if (this.lastImage != null) {
      let serverUrl = config.cloudinaryUrl;

      let targetPath = this.lastImage;

      let fileName = this.lastImage;

      let options: FileUploadOptions = {
        params: {
          file: fileName,
          upload_preset: "fx373lb4"
        }
      };

      const fileTrasfer: FileTransferObject = this.transfer.create();

      let loader = this._loader.create({
        content: "Uploading..."
      });
      loader.present();

      fileTrasfer.upload(targetPath, serverUrl, options).then(
        data => {
          let imageUrl = JSON.parse(data.response);
          this._sendData
            .update({ image: imageUrl.secure_url }, this.id)
            .subscribe((data: any) => {
              this.picture_url = data.image;
              this._storage.get("user").then(val => {
                let value = val;
                value.image = data.image;
                this._storage.set("user", value);
              });
              this.events.publish("change_image", data.image);
              loader.dismiss();
              this.showAlert("Successful", "Image has been changed");
            });
        },
        err => {
          loader.dismiss();
          console.error(err);
          this.presentToast("Error In Upload");
        }
      );
    } else {
      this.presentToast("Error, No new Image has been chosen !");
    }
  }

  public pathForImage(img) {
    if (img === null) {
      return "";
    } else {
      let path = cordova.file.dataDirectory;
      path = normalizeURL(path);
      return path + img;
    }
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: "top"
    });
    toast.present();
  }
  showAlert(title: string, message: string) {
    this.alert
      .create({
        title: title,
        subTitle: message,
        buttons: ["ok"]
      })
      .present();
  }
  editP(user) {
    const loader = this._loader.create({
      content: "Updating..."
    });
    loader.present();
    const { id, ...removeId } = user;
    this.editService.update(removeId, this.id).subscribe(
      (response: any) => {
        console.log('here');
        this._storage.set("user", response).then(() => {
          console.log('here');
          this.events.publish('onEdit', ()=>{
            console.log('published');
          });
          loader.dismiss();
        });
        
      },
      err => {
        let errorMessage = this.compileError(err);
        loader.dismiss();
        this.showAlert("Opps!", errorMessage);
      }
    );
  }
  getBanks(){
    try{
      this.userProvider.getBanks().subscribe((response: any) => {
        this.banks = response;
      })
    }catch(e){}
  }
  editProfile() {
    this.editP(this.user);
  }

  compileError(error: any): any {
    let errorMessage = "";
    let real_error = error.json().errors;
    for (let err in real_error) {
      errorMessage = errorMessage + "\n" + real_error[err][0];
    }
    return errorMessage.toString();
  }
}
