import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchooldirectoryPage } from './schooldirectory';
import { ComponentsModule } from '../../components/components.module';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [
    SchooldirectoryPage,
  ],
  imports: [
    IonicPageModule.forChild(SchooldirectoryPage),
    ComponentsModule,
    LazyLoadImageModule
  ],
})
export class SchooldirectoryPageModule {}
