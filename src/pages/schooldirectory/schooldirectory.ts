import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { DirectoryProvider } from "./../../providers/directory/directory";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";

@IonicPage()
@Component({
  selector: "page-schooldirectory",
  templateUrl: "schooldirectory.html"
})
export class SchooldirectoryPage {
  showFilter = false;
  featuredCenters: any[];
  loaded: Promise<boolean> = Promise.resolve(false);

  query: string;

  options: NativeTransitionOptions = {
    direction: "left",
    duration: 400,
    slowdownfactor: -1,
    iosdelay: 50,
    androiddelay: 50
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private directoryService: DirectoryProvider,
    private _loader: LoadingController,
    private alertCtrl: AlertController,
    private nativePage: NativePageTransitions
  ) {}
  ionViewWillLoad() {
    this.getFeaturedDirectory();
  }

  search() {
    if (this.query != null || undefined) {
      let loading = this._loader.create({
        content: "Searching..."
      });
      loading.present();
      this.directoryService
        .searchSchool(this.query)
        .subscribe((response: any) => {
          if (response.data.length != 0) {
            loading.dismiss();
            this.nativePage.slide(this.options);
            this.navCtrl.push("DirectorySearchPage", {
              result: response
            });
          } else {
            loading.dismiss();
            this.alertCtrl.create({
              subTitle: "Couldn't find a result matching your input",
              buttons: ['ok']
            }).present();
          }
        });
    }
  }
  getFeaturedDirectory() {
    let loading = this._loader.create({
      content: "Loading..."
    });
    loading.present();
    this.directoryService.getSchoolFeatured().subscribe(
      response => {
        this.featuredCenters = response;
        this.loaded = Promise.resolve(true);
        loading.dismiss();
      },
      err => {
        this.alertCtrl
          .create({
            subTitle: "Internet Connection is slow",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  loading.dismiss();
                  this.navCtrl.setRoot("HomePage");
                }
              }
            ]
          })
          .present();
      }
    );
  }
  openInfoPage(school) {
    this.navCtrl.push("SchoolDirectoryInfoPage", {
      school: school
    });
  }
}
