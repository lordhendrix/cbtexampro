import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  openPrepPage(){
    this.navCtrl.push('PrepCenterPage');
  }
  openSchoolDirectory(){
    this.navCtrl.push('SchooldirectoryPage');
  }
  openLeaderboard(){
    this.navCtrl.push('LeaderboardPage');
  }
  openQuizPage(){
    this.navCtrl.push('Quiz4cashPage');
  }
  openWalletPage(){
    this.navCtrl.push('WalletPage');
  }
  openTutorsPage(){
    this.navCtrl.push('TutordirectoryPage');
  }
}
