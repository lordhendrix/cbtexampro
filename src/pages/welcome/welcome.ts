import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { SessionProvider } from '../../providers/session/session';
import { TabsPage } from '../tabs/tabs';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
  @ViewChild(Slides) slides: Slides;
  showSkip = true;
  showNext = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private session: SessionProvider) {
  }

  ionViewDidLoad() {}
  
  slideChanged() {
    if(this.slides.isEnd()){
      this.showNext= true;
      this.showSkip = false;
    } else {
      this.showNext = false;
      this.showSkip = true;
    }
  }
  gotoHomePage(){
    this.session.welcomeView();
    this.navCtrl.setRoot(TabsPage);
  }

}
