import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Screenshot } from '@ionic-native/screenshot';
import { SocialSharing } from '@ionic-native/social-sharing';


@IonicPage()
@Component({
  selector: 'page-quizresult',
  templateUrl: 'quizresult.html',
})
export class QuizresultPage {
  result:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private screen: Screenshot, private social: SocialSharing) {
    this.result =  this.navParams.get('result');
  } 

  ionViewDidLoad() {
  }

  shareResult() {
    this.screen.URI(80).then((val) => {
      let message = `Hi friends I just  scored ${this.result.percentageScore}%, join me`;
      this.social.share(message, 'Prepare for text and exams with ease' , val.URI, 'https://cbtexampro.com');
    })    
  }

}
