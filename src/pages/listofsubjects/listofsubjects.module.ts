import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListofsubjectsPage } from './listofsubjects';

@NgModule({
  declarations: [
    ListofsubjectsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListofsubjectsPage),
  ],
})
export class ListofsubjectsPageModule {}
