import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { PrepexamProvider } from "../../providers/prepexam/prepexam";

@IonicPage()
@Component({
  selector: "page-postutme",
  templateUrl: "postutme.html"
})
export class PostutmePage {
  schools: any[];
  pageName = "POST UTME";
  loaded: Promise<boolean>;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private prepExamProvider: PrepexamProvider
  ) {}

  ionViewDidLoad() {
    let postUtme = this.navParams.get("exam");
    this.getSchools(postUtme.slug);
  }

  gotoExamSelectionPage(school){
    this.navCtrl.push("PrepexamtakingPage", {
      exam: school
    });
  }

  getSchools(slug) {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    this.prepExamProvider.getPostUtmeSchools(slug).subscribe(
      (response: any) => {
        this.schools = response;
        this.loaded = Promise.resolve(true);
        loader.dismiss();
      },
      err => {
        loader.dismiss();
        this.alertCtrl
          .create({
            subTitle: "Internet Connection is slow",
            buttons: [
              {
                text: "ok",
                handler: () => {
                  loader.dismiss();
                  this.navCtrl.pop();
                }
              }
            ]
          })
          .present();
      }
    );
  }
}
