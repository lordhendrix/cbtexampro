import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BlogProvider } from '../../providers/blog/blog';



@IonicPage()
@Component({
  selector: 'page-blog',
  templateUrl: 'blog.html',
})
export class BlogPage {
  loaded: Promise<boolean>;
  data: any[] = [];
  nexturl: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public blogService: BlogProvider, private alertCtrl: AlertController ) {
  }

  ionViewDidLoad() {
    this.getBlog();
  }
  openblog(content){
    this.navCtrl.push('BloginfoPage', {
      post: content
    });
  }

  getBlog(){
    this.blogService.getAllArticles().subscribe((response : any) => {
      this.data = this.transformData(response.data);
      this.nexturl = response.next_page_url;
      this.loaded = Promise.resolve(true);
    }, (err) => {
      console.log(err);
      this.alertCtrl.create({
        subTitle: 'Internet Connection is slow',
        buttons: [{
          text: 'ok',
          handler: () => {
            this.loaded = Promise.resolve(true);
            this.navCtrl.setRoot('HomePage');
          }
        }]
      }).present();
    });
  }

  transformData(data: any[]) {
    const transformedData = [];
    for(let i = 0; i < data.length; i++) {
      var str = data[i].content
      let temp = document.createElement('div');
      let frag = document.createDocumentFragment();
      temp.innerHTML = str;
      frag.appendChild(temp);
      transformedData.push({image: temp.getElementsByTagName('img')[0].src, ...data[i]}​​​​)
    }
    return transformedData;
  }
  onScroll(event){
    if(this.nexturl != null){
      this.blogService.getMoreArticles(this.nexturl).toPromise().then(res => res.json()).then((response) => {
        this.data.push(response.data);
        event.complete();
      })
    } else {
      event.complete();
    }
  }

}
