import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogPage } from './blog';
import { Timesago } from '../../pipes/moment';

@NgModule({
  declarations: [
    BlogPage,
    Timesago
  ],
  imports: [
    IonicPageModule.forChild(BlogPage),
  ],
})
export class BlogPageModule {}
