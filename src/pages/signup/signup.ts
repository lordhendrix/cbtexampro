import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  MenuController,
  LoadingController,
  AlertController,
  Events
} from "ionic-angular";

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { SessionProvider } from '../../providers/session/session';
import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  passwordType: string = "password";
  passwordIcon: string = "eye-off";
  emailRegEx = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
  showFooter = true;
  private register: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private formBuilder: FormBuilder,
    private loader: LoadingController,
    private auth: AuthenticationProvider,
    private session: SessionProvider,
    private alert: AlertController,
    private keyboard: Keyboard,
    private event: Events
  ) {
    this.register = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(this.emailRegEx)]],
      phone: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.keyboard.onKeyboardShow().subscribe(() => {
      this.showFooter = false;
    });
    this.keyboard.onKeyboardHide().subscribe(() => {
      this.showFooter = true;
    })
  }
  ionViewDidEnter() {
    this.menu.swipeEnable(false);
  }

  ionViewWillLeave() {
    this.menu.swipeEnable(true);
  }

  hideShowPassword() {
    this.passwordType = this.passwordType === "text" ? "password" : "text";
    this.passwordIcon = this.passwordIcon === "eye-off" ? "eye" : "eye-off";
  }
  openLoginPage() {
    this.navCtrl.push("LoginPage");
  }
  createAccount(){
    let loading =  this.loader.create({
      content: 'Creating Account...'
    })
    loading.present();
    this.auth.register(this.register.value).subscribe((response) => {
      this.session.newUser(response.data).then(() => {
        this.event.publish('onLogin');
      });
      loading.dismiss();
      this.navCtrl.pop();
    }, (err) => {
      loading.dismiss();
      let error = this.compileError(err);
      this.showAlert('Opps', error);
    })
  }
  compileError(error : any) : any {
    let errorMessage = "";
    let real_error = error.json().errors
    for(let err in real_error){
     errorMessage = errorMessage + "\n" + real_error[err][0];
    }
    return errorMessage.toString();
  }

  showAlert(title: string ,message: string){
    let alert = this.alert.create({
      title: title,
      subTitle : message,
      buttons: ['ok']
    });
    alert.present();
  }
}
