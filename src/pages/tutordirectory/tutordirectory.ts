import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController
} from "ionic-angular";
import { DirectoryProvider } from "./../../providers/directory/directory";
import {
  NativePageTransitions,
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";

@IonicPage()
@Component({
  selector: "page-tutordirectory",
  templateUrl: "tutordirectory.html"
})
export class TutordirectoryPage {
  featured: any[];
  loaded: Promise<boolean>;
  nextUrl: string;

  query: string;
  options: NativeTransitionOptions = {
    direction: "left",
    duration: 400,
    slowdownfactor: -1,
    iosdelay: 50,
    androiddelay: 50
  };

  constructor(
    public navCtrl: NavController,
    private nativePage: NativePageTransitions,
    public navParams: NavParams,
    private directoryProvider: DirectoryProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {}

  ionViewDidLoad() {
    this.getFeaturedTutors();
  }

  search() {
    if (this.query != null || undefined) {
      let loading = this.loadingCtrl.create({
        content: "Searching..."
      });
      loading.present();
      this.directoryProvider.searchTutor(this.query).subscribe(
        (response: any) => {
          console.log(response);
          if (response.data.length != 0) {
            loading.dismiss();
            this.nativePage.slide(this.options);
            this.navCtrl.push("TutorSearchPage", {
              result: response
            });
          } else {
            loading.dismiss();
            this.alertCtrl
              .create({
                subTitle: "Couldn't find a result matching your input",
                buttons: ["ok"]
              })
              .present();
          }
        },
        err => {
          loading.dismiss();
          this.alertCtrl
            .create({
              subTitle: "Couldn't find a result matching your input",
              buttons: ["ok"]
            })
            .present();
        }
      );
    }
  }
  openProfile(selectedTutor) {
    this.navCtrl.push("TutorsProfilePage", {
      tutor: selectedTutor
    });
  }

  getFeaturedTutors() {
    let loading = this.loadingCtrl.create({
      content: "Loading..."
    });
    loading.present();
    this.directoryProvider.getTutorFeatured().subscribe(
      (response: any) => {
        this.featured = response;
        this.loaded = Promise.resolve(true);
        loading.dismiss();
      },
      err => {
        this.loaded = Promise.resolve(false);
        loading.dismiss();
        this.alertCtrl
          .create({
            subTitle: "Couldn't find a result matching your input",
            buttons: ["ok"]
          })
          .present();
      }
    );
  }
}
