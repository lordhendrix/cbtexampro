import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutordirectoryPage } from './tutordirectory';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [
    TutordirectoryPage,
  ],
  imports: [
    LazyLoadImageModule,
    IonicPageModule.forChild(TutordirectoryPage),
  ],
})
export class TutordirectoryPageModule {}
