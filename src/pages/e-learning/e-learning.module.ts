import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ELearningPage } from './e-learning';
import { DirectivesModule } from '../../directives/directives.module';
import { PipesModule } from '../../pipes/pipes.module';



@NgModule({
  declarations: [
    ELearningPage,
   ],
  imports: [
    DirectivesModule,
    PipesModule,
    IonicPageModule.forChild(ELearningPage),
  ],
})
export class ELearningPageModule {}
