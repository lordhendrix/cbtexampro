import { ElearningProvider } from './../../providers/elearning/elearning';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-e-learning',
  templateUrl: 'e-learning.html',
})
export class ELearningPage {
  categories: any;
  categoryKeys = Object.keys;
  categoryKeyLength;
  keyLength;
  loaded: Promise<boolean>;
  constructor(public navCtrl: NavController, public navParams: NavParams, private _elearning: ElearningProvider, private loader: LoadingController, private alertCtrl: AlertController) {
    this.getCategories()
  }

  ionViewDidLoad() {}
  
  getCategories(){
    let loading = this.loader.create({
      content : 'Loading...'
    });
    loading.present();
    this._elearning.getCategories().subscribe((response: any) => {
      this.categories = response;
      this.loaded = Promise.resolve(true);
      loading.dismiss();
    }, (err) => {
      this.alertCtrl.create({
        subTitle: 'Internet Connection is slow',
        buttons: [{
          text: 'ok',
          handler: () => {
            loading.dismiss();
            this.navCtrl.setRoot('HomePage');
          }
        }]
      }).present();
    })
  }

  openSubjects(groupname){
    this.navCtrl.push('ElearningsubjectsPage', {
      category : groupname
    });
  }

}
