import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-calculator',
  templateUrl: 'calculator.html',
})
export class CalculatorPage {
result = '';    
lastPress = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
  }

  calculate(btn) {
    if (btn == 'C') {
        this.result = '';
        this.lastPress = false;
    }
    else if (btn == '=') {
        if (this.result == '') {
            return;
        }

        try {
            this.result = eval(this.result).toFixed(2);
            this.lastPress = true;
        } catch (error) {
            this.showAlert();
            this.result = '';
        }
    }
    else {
        if(this.lastPress){
            this.result = '';
        }
        this.result += btn;
        this.lastPress = false;
    }
}

showAlert() {
        this.alertCtrl.create({
            title: 'Malformed input',
            subTitle: 'Ooops, please try again...',
            buttons: ['Dismiss']
        }).present();
    }
}
