import { SessionProvider } from "./../../providers/session/session";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, App, Events, AlertController } from "ionic-angular";
import { ActionSheetController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { UserProvider } from "../../providers/user/user";

@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  name: string;
  picture_url: string;
  public profile: string = "exams";
  public categories: Array<string> = ["exams", "wallet"];
  loggedIn: boolean;
  loaded: Promise<boolean>;
  subscriptions: any[];
  next_url: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public actionSheet: ActionSheetController,
    private _storage: Storage,
    private app: App,
    private userProvider: UserProvider,
    private event: Events,
    private alertCtrl: AlertController
  ) {
    this.event.subscribe('onEdit', () => {
      this._storage.get('user').then((val) => {
        this.name = val.name;
      })
    });
    this.event.subscribe('change_image', (image) => {
      this.picture_url = image;
    })
  }

  ionViewWillEnter() {
    this.loggedIn = true;
    this._storage.get("user").then(val => {
      if (val != null) {
        this.loggedIn = true;
        this.name = val.name;
        this.picture_url = val.image;
      } else {
        this.loggedIn = false;
      }
    });
    this.getSubs();
  }

  onEditClick() {
    const actionsheet = this.actionSheet.create({
      title: "Options",
      buttons: [
        {
          text: "Edit Profile",
          handler: () => {
            this.navCtrl.push("EditprofilePage");
          }
        },
        {
          text: "Help",
          handler: () => {
            this.navCtrl.push("HelpPage");
          }
        },
        {
          text: "LogOut",
          handler: () => {
            this.logOut();
          }
        }
      ]
    });
    actionsheet.present();
  }

  formatDate(obj) {
    return new Date(obj.replace(/-/g, "/"));
  }

  toExamPage(){
    this.navCtrl.setRoot('PrepCenterPage');
  }
  getSubs() {
    this._storage.get("user").then(val => {
      if (val != null) {
        this.userProvider
          .getUserSubscriptions(val.id)
          .subscribe((response: any) => {
            this.loaded = Promise.resolve(true);
            this.subscriptions = response.data;
            this.next_url = response.next_page_url;
          }, (err) => {
            this.alertCtrl.create({
              subTitle: 'Network Connection is slow!',
              buttons: ['ok']
            })
          });
      }
    });
  }

  gotoExamTakingPage(exam) {
    this.navCtrl.push("PrepexamtakingPage", {
      exam: exam
    });
  }

  onScroll(event){
    if(this.next_url != null){
      this.userProvider.getMoreSubscriptions(this.next_url).toPromise().then(res => res.json()).then((response) => {
        this.next_url = response.next_page_url;
        response.data.forEach(element => {
          this.subscriptions.push(element);
        });
        event.complete();
      })
    } else {
      event.complete();
    }
  }
  logOut() {
    this._storage.remove('user');
    this.event.publish('onLogout');
    this.app.getRootNav().setRoot("WelcomePage");
  }
}
