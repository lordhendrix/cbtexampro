import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BloginfoPage } from './bloginfo';

@NgModule({
  declarations: [
    BloginfoPage,
  ],
  imports: [
    IonicPageModule.forChild(BloginfoPage),
  ],
})
export class BloginfoPageModule {}
