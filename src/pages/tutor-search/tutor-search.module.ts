import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TutorSearchPage } from './tutor-search';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [
    TutorSearchPage,
  ],
  imports: [
    LazyLoadImageModule, 
    IonicPageModule.forChild(TutorSearchPage),
  ],
})
export class TutorSearchPageModule {}
