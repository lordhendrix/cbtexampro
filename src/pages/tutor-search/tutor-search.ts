import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {
  NativeTransitionOptions
} from "@ionic-native/native-page-transitions";
import { DirectoryProvider } from '../../providers/directory/directory';

@IonicPage()
@Component({
  selector: 'page-tutor-search',
  templateUrl: 'tutor-search.html',
})
export class TutorSearchPage {
  searchedTutor: any[];
  next_url: string;

  options: NativeTransitionOptions = {
    direction: "left",
    duration: 400,
    slowdownfactor: -1,
    iosdelay: 50,
    androiddelay: 50
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, private directoryService: DirectoryProvider) {
    let result = this.navParams.get('result');
    this.searchedTutor = result.data;
    this.next_url = result.next_page_url;
  }

  ionViewDidLoad() {}

  openProfile(selectedTutor){
    this.navCtrl.push('TutorsProfilePage', {
      tutor: selectedTutor
    });
  }

  doInfinite(event) {
    if (this.next_url != null) {
      this.directoryService
        .getMoreSearchedTutor(this.next_url)
        .subscribe((response: any) => {
          this.next_url = response.next_page_url;
          response.data.forEach(element => {
            this.searchedTutor.push(element);
          });
          event.complete();
        });
    } else{
      event.complete();
    }
  }

}
