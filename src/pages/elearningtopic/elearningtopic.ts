import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ElearningProvider } from '../../providers/elearning/elearning';



@IonicPage()
@Component({
  selector: 'page-elearningtopic',
  templateUrl: 'elearningtopic.html',
})
export class ElearningtopicPage {
  topics: any[];
  pageName: string;
  loaded: Promise<boolean>;
  constructor(public navCtrl: NavController, public navParams: NavParams, private elearningService: ElearningProvider, private loadingCtrl: LoadingController) {
    let subject = this.navParams.get('subject');
    console.log(subject.id, 'topic id');
    this.pageName = subject.name;
    this.getTopics(subject.id);
  }

  ionViewDidLoad() {
  }

  getTopics(id){
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    })
    loading.present();
    this.elearningService.getTopics(id).subscribe((response) => {
      console.log(response);
      this.topics = response;
      this.loaded = Promise.resolve(true);
      loading.dismiss();
    })
  }

  openTopicDescription(topic){
    this.navCtrl.push('ElearningDescriptionPage', {
      topic: topic
    })
  }
}
