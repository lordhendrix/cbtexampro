import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElearningtopicPage } from './elearningtopic';

@NgModule({
  declarations: [
    ElearningtopicPage,
  ],
  imports: [
    IonicPageModule.forChild(ElearningtopicPage),
  ],
})
export class ElearningtopicPageModule {}
