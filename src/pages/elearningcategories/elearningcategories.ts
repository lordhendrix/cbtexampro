import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { PrepexamProvider } from '../../providers/prepexam/prepexam';



@IonicPage()
@Component({
  selector: 'page-elearningcategories',
  templateUrl: 'elearningcategories.html',
})
export class ElearningcategoriesPage {
  categories: any[];
  pageName: string;
  loaded: Promise<boolean>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private elearningProvider: PrepexamProvider, private loadingCtrl: LoadingController) {
    let groupname = this.navParams.get('category');
    this.pageName = groupname;
    this.getGroups(groupname);
  } 

  ionViewDidLoad() {
  }

  getGroups(groupname){
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    })
    loading.present();
    this.elearningProvider.getGroupExam(groupname).subscribe((response: any) => {
      this.categories = response.data;
      this.loaded = Promise.resolve(true);
      loading.dismiss();
    }, (err) => {

    })
  }

  gotoSubjects(category) {
    this.navCtrl.push('ElearningsubjectsPage', {
      category: category
    });
  }

}
