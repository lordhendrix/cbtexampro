import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElearningcategoriesPage } from './elearningcategories';

@NgModule({
  declarations: [
    ElearningcategoriesPage,
  ],
  imports: [
    IonicPageModule.forChild(ElearningcategoriesPage),
  ],
})
export class ElearningcategoriesPageModule {}
