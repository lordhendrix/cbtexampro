import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DirectorySearchPage } from './directory-search';
import { LazyLoadImageModule } from 'ng-lazyload-image';


@NgModule({
  declarations: [
    DirectorySearchPage,
  ],
  imports: [
    LazyLoadImageModule,
    IonicPageModule.forChild(DirectorySearchPage),
  ],
})
export class DirectorySearchPageModule {}
