import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { DirectoryProvider } from "../../providers/directory/directory";

@IonicPage()
@Component({
  selector: "page-directory-search",
  templateUrl: "directory-search.html"
})
export class DirectorySearchPage {
  featuredCenters: any[];
  next_url: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private directoryService: DirectoryProvider
  ) {
    let result = this.navParams.get("result");
    this.featuredCenters = result.data;
    this.next_url = result.next_page_url;
  }

  ionViewDidLoad() {}

  openInfoPage(school) {
    this.navCtrl.push("SchoolDirectoryInfoPage", {
      school: school
    });
  }

  doInfinite(event) {
    if (this.next_url != null) {
      this.directoryService
        .getMoreSearchedSchool(this.next_url)
        .subscribe((response: any) => {
          this.next_url = response.next_page_url;
          response.data.forEach(element => {
            this.featuredCenters.push(element);
          });
          event.complete();
        });
    } else{
      event.complete();
    }
  }
}
