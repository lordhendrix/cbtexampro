import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ElearningProvider } from '../../providers/elearning/elearning';



@IonicPage()
@Component({
  selector: 'page-elearningsubjects',
  templateUrl: 'elearningsubjects.html',
})
export class ElearningsubjectsPage {
  pageName: string;
  subjects: any[];
  loaded: Promise<boolean>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private eLearningService: ElearningProvider, private loadingCtrl: LoadingController) {
    let category = this.navParams.get('category');
    this.pageName = category.name;
    this.getSubjects(category.id);
  }

  ionViewDidLoad() {
  }

  getSubjects(id) {
    let loading = this.loadingCtrl.create({
      content: 'Loading...'
    })
    loading.present();
    this.eLearningService.getSubjects(id).subscribe((response) => {
      console.log(response);
      this.subjects = response;
      this.loaded = Promise.resolve(true);
      loading.dismiss();
    })
  }
  openTopicsPage(subject){
    this.navCtrl.push('ElearningtopicPage', {
      subject: subject
    })
  }

  openTopicPage(topic){
    this.navCtrl.push('ElearningDescriptionPage', {
      topic: topic
    })
  }
}
