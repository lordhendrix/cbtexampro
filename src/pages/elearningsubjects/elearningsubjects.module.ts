import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElearningsubjectsPage } from './elearningsubjects';

@NgModule({
  declarations: [
    ElearningsubjectsPage,
  ],
  imports: [
    IonicPageModule.forChild(ElearningsubjectsPage),
  ],
})
export class ElearningsubjectsPageModule {}
