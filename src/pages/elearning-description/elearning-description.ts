import { InappbrowserProvider } from "./../../providers/inappbrowser/inappbrowser";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  AlertController,
  Platform
} from "ionic-angular";
import { ElearningProvider } from "../../providers/elearning/elearning";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";

declare var cordova;

@IonicPage()
@Component({
  selector: "page-elearning-description",
  templateUrl: "elearning-description.html"
})
export class ElearningDescriptionPage {
  pageName;
  documents: any[];
  loaded: Promise<boolean>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private elearningProvider: ElearningProvider,
    private loadingCtrl: LoadingController,
    private browser: InappbrowserProvider,
    private fileTransfer: FileTransfer,
    private alertController: AlertController,
    private platform: Platform
  ) {
    let topic = this.navParams.get("topic");
    this.pageName = topic.name;
    this.getDocuments(topic.id);
  }

  ionViewDidLoad() {}

  getDocuments(id) {
    let loading = this.loadingCtrl.create({
      content: "Loading..."
    });
    loading.present();
    this.elearningProvider.getTopicDocuments(id).subscribe((response: any) => {
      console.log(response);
      this.documents = response.data;
      this.loaded = Promise.resolve(true);
      loading.dismiss();
    });
  }
  openLink(link) {
    this.browser.openBrowser(link);
  }
  downloadFile(link, filetype, filename) {
    let fail = () => {
      this.alertController
        .create({
          subTitle: "Unable to download",
          buttons: ["ok"]
        })
        .present();
    };
    let success = () => {
      this.alertController
        .create({
          subTitle: "Downloading",
          buttons: ["ok"]
        })
        .present();
    };
    if (this.platform.is("android")) {
      cordova.plugins.DownloadManager.download(link, success, fail);
    } else {
      const storageDirectory = cordova.file.documentsDirectory;
      const loading = this.loadingCtrl.create({
        content: 'Downloading...'
      });
      loading.present();
      const fileTransfer: FileTransferObject = this.fileTransfer.create();
      fileTransfer.download(link, storageDirectory + filename + '.' + filename).then((entry) => {
        loading.dismiss();
        const alertSuccess = this.alertController.create({
          title: 'Download done',
          subTitle: `file was successfully downloaded to: ${entry.toURL()}`,
          buttons: ['ok']
        })
        alertSuccess.present();
      }).catch((err) => {
        loading.dismiss();
        const alertError = this.alertController.create({
          subTitle: 'Error, unable to download',
          buttons: ['ok']
        })
        alertError.present();
      })


      /*
      downloader.init({ folder: "/", unzip: true , fileSystem: cordova.file.documentsDirectory});
      downloader.get(link);
      document.addEventListener(
        "DOWNLOADER_error" || "DOWNLOADER_downloadError",
        function(event) {
          this.alertController
            .create({
              subTitle: "Unable to download",
              buttons: ["ok"]
            })
            .present();
        }
      );

      document.addEventListener("DOWNLOADER_downloadSuccess", function(event) {
        this.alertController
          .create({
            subTitle: "Download done",
            buttons: ["ok"]
          })
          .present();
      });
      */
    }
    /*
    let loading = this.loadingCtrl.create({
      content: "Downloading..."
    });
    loading.present();
    let fileTransferVar: FileTransferObject = this.fileTransfer.create();
    const rootDirectory = "file:///storage/emulated/0/";
    this.file
      .createDir(rootDirectory, "cbtexampro", false)
      .then(dir => {
        let path = dir + "/" + filename + Date.now() + "." + filetype;
        fileTransferVar.download(link, path).then(
          entry => {
            loading.dismiss();
            this.alertController
              .create({
                subTitle: "Download Done",
                buttons: ["ok"]
              })
              .present();
          },
          err => {
            console.log(err);
            this.alertController
              .create({
                subTitle: "Unable to download",
                buttons: ["ok"]
              })
              .present();
          }
        );
      })
      .catch(err => {
        console.log(err);
        const dir = rootDirectory + "cbtexampro";
        let path = dir + "/" + filename + Date.now() + "." + filetype;
        fileTransferVar.download(link, path).then(
          entry => {
            loading.dismiss();
            this.alertController
              .create({
                subTitle: "Download Done",
                buttons: ["ok"]
              })
              .present();
          },
          err => {
            console.log(err);
            this.alertController
              .create({
                subTitle: "Unable to download",
                buttons: ["ok"]
              })
              .present();
          }
        );
      });
      */
  }
}
