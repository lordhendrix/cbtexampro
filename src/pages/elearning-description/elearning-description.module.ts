import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElearningDescriptionPage } from './elearning-description';

@NgModule({
  declarations: [
    ElearningDescriptionPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(ElearningDescriptionPage),
  ],
})
export class ElearningDescriptionPageModule {}
