export interface login {
    email: string;
    password: string;
}

export interface register extends login{
    phone: string;
    name: string;
}

export interface user {

}